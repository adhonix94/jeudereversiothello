#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QGraphicsScene>
#include <QLineEdit>
#include <QMainWindow>
#define TAILLE 50
#define NBR_CELL 8


class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private slots:
    void slotJouer();
    void dessinerPiece(int aCol, int aLigne, int aJoueurId);
    void slotQuitter();
    void slotPasserSonTour();
    void slotTerminerLaPartie();
    bool mouvementValide(int joueur, int ligne, int colonne);
    void  placerUnPion(int joueur, int ligne, int colonne);
    bool finDeLaPartie() ;
private:
int joueur = 1;
    const QString ETIQUETTE = "ABCDEFGH";
    int tableau[NBR_CELL][NBR_CELL];

    QLineEdit *choixXY;
    QGraphicsScene *scene;

    void dessinerJeu();
    void initialiserJeu();
};
#endif // MAINWINDOW_H
