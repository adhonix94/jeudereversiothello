#include "mainwindow.h"

#include <QFormLayout>
#include <QGraphicsRectItem>
#include <QGraphicsView>
#include <QPushButton>
#include <QMenuBar>
#include <QApplication>


MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
{
    QMenu *fichierMenu = menuBar()->addMenu(tr("&Fichier"));
    fichierMenu->addAction("Quitter", this, &MainWindow::slotQuitter);

    QWidget *fenetre = new QWidget(this);
    QVBoxLayout *grille = new QVBoxLayout();
    fenetre->setLayout(grille);

    // 1
    scene = new QGraphicsScene;
    initialiserJeu();
    QGraphicsView *view = new QGraphicsView(scene);
    grille->addWidget(view);


    choixXY = new QLineEdit;
    choixXY->setFocus();
    choixXY->setInputMask("A9");
    QFormLayout *xyChoisieForm = new QFormLayout;
    xyChoisieForm->addRow("XY", choixXY);
    grille->addLayout(xyChoisieForm);
    choixXY->setCursorPosition(0);

    QPushButton *boutonJouer = new QPushButton("Jouer");
    grille->addWidget(boutonJouer);
    connect(boutonJouer, &QPushButton::clicked, this, &MainWindow::slotJouer);

    QPushButton *boutonPasserSonTour = new QPushButton("Passer son tour");
    grille->addWidget(boutonPasserSonTour);
    connect(boutonPasserSonTour, &QPushButton::clicked, this, &MainWindow::slotPasserSonTour);

    QPushButton *boutonTerminerLaPartie = new QPushButton("Terminer la partie");
    grille->addWidget(boutonTerminerLaPartie);
    connect(boutonTerminerLaPartie, &QPushButton::clicked, this, &MainWindow::slotTerminerLaPartie);

    setCentralWidget(fenetre);

}



MainWindow::~MainWindow()
{
}

void MainWindow::initialiserJeu()
{
    
    for(int i=0; i<NBR_CELL; i++) {
        for(int j=0; j<NBR_CELL; j++) {
            tableau[i][j] = 0;
        }
    }

    //Blanc
    tableau[3][3] = 2;
    tableau[4][4] = 2;
    
    //Noir
    tableau[3][4] = 1;
    tableau[4][3] = 1;

    dessinerJeu();

    //Joueur qui doit commencer
    if (joueur == 1) {
        qDebug() << "C'est au tour du joueur noir";
    } else if (joueur == 2) {
        qDebug() << "C'est au tour du Joueur blanc";
    }
}


void MainWindow::dessinerJeu() {
    scene->clear();

    for(int i =0; i<NBR_CELL; i++) {
        // 2
        // dessine les coordonnées
        QGraphicsTextItem *txt = new QGraphicsTextItem();
        txt->setPlainText(ETIQUETTE[i]);
        txt->setPos((i+1)*(TAILLE)+15, 20);
        scene->addItem(txt);

        //suite de chiffres
        txt = new QGraphicsTextItem();
        txt->setPlainText(QString::number(i+1));
        txt->setPos(20, (i+1)*(TAILLE) + 15);
        scene->addItem(txt);

        //chiffre
        txt = new QGraphicsTextItem();
        txt->setPlainText(QString::number(i+1));
        txt->setPos((NBR_CELL + 1) * (TAILLE) + 20, (i+1)*(TAILLE) + 15);
        scene->addItem(txt);

        txt = new QGraphicsTextItem();
        txt->setPlainText(ETIQUETTE[i]);
        txt->setPos((i+1)*(TAILLE)+15, (NBR_CELL + 1) * (TAILLE) + 20);
        scene->addItem(txt);

        // dessine les cases
        for(int j=0; j<NBR_CELL; j++) {
            QGraphicsRectItem *rectItem = // construis un rectangle
                    new QGraphicsRectItem(QRectF((i+1)*TAILLE, (j+1)*TAILLE, TAILLE, TAILLE));// calcule l'emplacement de chaque cases en X et Y.

            rectItem->setBrush(Qt::green);
            scene->addItem(rectItem);
            //dessine les pieces
            if(tableau[i][j] != 0)
                {
                    //qDebug() << tableau[i][j];// affiche en console.
                    dessinerPiece(i,j,tableau[i][j]);// inserer les coordonnées
                }

        }

    }
}

void MainWindow::dessinerPiece(int x, int y, int aJoueurId) {
    int RAYON = 16;
    
    //dessine les ronds dans le tableau et calcul la couleur de chaque rond en fonction de sa position.
    QGraphicsEllipseItem *rond = new QGraphicsEllipseItem((x+1)*TAILLE+RAYON/2, (y+1)*TAILLE+RAYON/2, RAYON*2, RAYON*2);

        QGraphicsPixmapItem *item;
        if(aJoueurId==1) {
            rond->setBrush(Qt::black);
        } else if(aJoueurId==2) {
            rond->setBrush(Qt::white);
        }
        rond->setScale(1);
        scene->addItem(rond);
}


void MainWindow::slotJouer() {
    if(!(choixXY->text().isEmpty())) {
        QString colTxt = choixXY->text()[0];
        QString ligneTxt = choixXY->text()[1];
        int col = ETIQUETTE.indexOf(colTxt.toUpper());
        int ligne = ligneTxt.toInt()-1;

        if(tableau[col][ligne]==0
                && col>=0 && col < NBR_CELL
                && ligne >=0 && ligne < NBR_CELL) {

            // Vérifier si le mouvement est valide
            bool mouvementEstValide = mouvementValide(joueur, ligne, col);
            if (mouvementEstValide) {

                //Placer un pion et changer les couleurs des pions capturés
                placerUnPion(joueur, ligne, col);

                //changer de joueur
                joueur =  ((joueur)%2) +1;

                if (joueur == 1) {
                    qDebug() << "C'est au tour du joueur noir";
                } else if (joueur == 2) {
                    qDebug() << "C'est au tour du joueur blanc";
                }
            } else {
                qDebug() << "Mouvement invalide. Veuillez réessayer.";
            }

            //Dessiner le jeu
            dessinerJeu();

        } else {
            // 5
            qDebug() << "Saisie invalide. Veuillez réessayer.";
        }

        if (finDeLaPartie()) {
            slotTerminerLaPartie();
        }

    }
    choixXY->clear();
    choixXY->setFocus();
    choixXY->setCursorPosition(0);
}

bool MainWindow::mouvementValide(int joueur, int ligne, int colonne)
{
    // Vérifier si la case est vide
    if (tableau[colonne][ligne] != 0) {
        return false;
    }

    // Vérifier si le coup capture des pièces adverses
    bool captures = false;
    for (int i = -1; i <= 1; ++i) { // Parcourir les directions horizontales
        for (int j = -1; j <= 1; ++j) { // Parcourir les directions verticales
            if (i == 0 && j == 0) {
                continue; // Ignorer la case actuelle
            }

            int nouvelleLigne = ligne + i;
            int nouvelleColonne = colonne + j;

            if (nouvelleLigne >= 0 && nouvelleLigne < NBR_CELL 
            && nouvelleColonne >= 0 && nouvelleColonne < NBR_CELL 
            && tableau[nouvelleColonne][nouvelleLigne] != 0 && tableau[nouvelleColonne][nouvelleLigne] != joueur) {
                // Si la case voisine contient une pièce adverse, chercher s'il y a une chaîne de pièces adverses dans cette direction qui peut être capturée

                int tempLigne = nouvelleLigne + i;
                int tempColonne = nouvelleColonne + j;

                while (tempLigne >= 0 && tempLigne < NBR_CELL 
                        && tempColonne >= 0 && tempColonne < NBR_CELL
                        && tableau [tempColonne][tempLigne] != 0) {

                    if (tableau [tempColonne][tempLigne] == joueur) {
                        // Si la chaîne est terminée par une pièce du joueur courant, le coup capture des pièces adverses
                        captures = true;
                        break;
                    }

                    tempLigne += i;
                    tempColonne += j;
                }

            }

        }
    }

    // Si aucune chaîne de pièces adverses n'a été capturée, le coup n'est pas valide
    if (!captures) {
        return false;
    }

    // Sinon, le coup est valide
    return true;
    
}

void MainWindow::placerUnPion(int joueur, int ligne, int colonne)
{
    // Placer la pièce du joueur sur la case choisie
    tableau [colonne][ligne] = joueur;

    // Capturer les pièces adverses dans chaque direction où c'est possible
    for (int i = -1; i <= 1; ++i) { // Parcourir les directions horizontales
        for (int j = -1; j <= 1; ++j) { // Parcourir les directions verticales
            if (i == 0 && j == 0) {
                // Ignorer la case actuelle
                continue; 
            }

            int nouvelleLigne = ligne + i;
            int nouvelleColonne = colonne + j;
            if (nouvelleLigne >= 0 && nouvelleLigne < NBR_CELL 
                && nouvelleColonne >= 0 && nouvelleColonne < NBR_CELL 
                && tableau [nouvelleColonne][nouvelleLigne] != 0
                && tableau[nouvelleColonne][nouvelleLigne] != joueur) {

                // Si la case voisine contient une pièce adverse, chercher s'il y a une chaîne de pièces adverses dans cette direction qui peut être capturée
                int tempLigne = nouvelleLigne + i;
                int tempColonne = nouvelleColonne + j;
                while (tempLigne >= 0 && tempLigne < NBR_CELL 
                        && tempColonne >= 0 && tempColonne < NBR_CELL 
                        && tableau [tempColonne][tempLigne] != 0) {

                    if (tableau [tempColonne][tempLigne] == joueur) {

                        // Si la chaîne est terminée par une pièce du joueur courant, capturer les pièces adverses dans cette direction
                        int captureLigne = nouvelleLigne;
                        int captureColonne = nouvelleColonne;

                        while (captureLigne != tempLigne || captureColonne != tempColonne) {
                            tableau [captureColonne][captureLigne] = joueur;
                            captureLigne += i;
                            captureColonne += j;
                        }
                        break;
                    }
                    tempLigne += i;
                    tempColonne += j;
                }
            }
        }
    }
}

bool MainWindow::finDeLaPartie() {
    int nombreCasesVide = 0;
    int nombrePionNoir = 0;
    int nombrePionBlanc = 0;

    // Parcourir toutes les cases du plateau de jeu pour compter les cases vides, noires et blanches
    for (int ligne = 0; ligne < NBR_CELL; ligne++) {
        for (int colonne = 0; colonne < NBR_CELL; colonne++) {
            if (tableau[colonne][ligne] == 0) {
                nombreCasesVide++;
            } else if (tableau[colonne][ligne] == 1) {
                nombrePionNoir++;
            } else if (tableau[colonne][ligne] == 2) {
                nombrePionBlanc++;
            }
        }
    }

    // Si le plateau de jeu est plein ou si aucun joueur n'a de pion sur le plateau, la partie est terminée
    if (nombreCasesVide == 0 || (nombrePionNoir == 0 && nombrePionBlanc == 0)) {
        if (nombrePionNoir > nombrePionBlanc) {
            qDebug() << "Le joueur noir a gagné avec" << nombrePionNoir << "pions";
        } else if (nombrePionBlanc > nombrePionNoir) {
            qDebug() << "Le joueur blanc a gagné avec" << nombrePionBlanc << "pions";
        } else {
            qDebug() << "La partie est nulle";
        }
        return true;
    }

    // Si l'un des joueurs n'a plus de pions sur le plateau, la partie est terminée
    if (nombrePionNoir == 0 || nombrePionBlanc == 0) {
        if (nombrePionNoir == 0) {
            qDebug() << "Le joueur blanc a gagné";
        } else {
            qDebug() << "Le joueur noir a gagné";
        }
        return true;
    }

    // Sinon, la partie n'est pas terminée
    return false;
}

void MainWindow::slotPasserSonTour(){
    joueur =  ((joueur)%2) +1;
    if (joueur == 1) {
        qDebug() << "Le joueur noir a passé son tour.";
    } else if (joueur == 2) {
        qDebug() << "Le joueur blanc a passé son tour.";
    }
}

void MainWindow::slotTerminerLaPartie(){
    qDebug() << "Partie terminée";
    initialiserJeu();
}

void MainWindow::slotQuitter()
{
    QApplication::quit();
}




